import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import urllib.request  # used to download dust opacity and molecular line data
import os
import subprocess as sp
import radmc3dPy
from scipy.interpolate import griddata

# Some natural constants
#
au  = 1.49598e13     # Astronomical Unit       [cm]
pc  = 3.08572e18     # Parsec                  [cm]
ms  = 1.98892e33     # Solar mass              [g]
ts  = 5.78e3         # Solar temperature       [K]
ls  = 3.8525e33      # Solar luminosity        [erg/s]
rs  = 6.96e10        # Solar radius            [cm]
gg  = 6.674e-08      # Gravitational constant  [cm^3/g/s^2]
mp  = 1.6726e-24     # Proton mass [g]

# List of ortho-para species
op_spec = {'h2o': 3/4}  # o:p = 3:1

class radmc3dmodel:
    
    params = None               # model parameters
    rri = np.nan                # radius in spherical coordinate system
    thi = np.nan                # inclination in spherical system
    phi = np.array([0.,2*np.pi])# azimuth - assume symmetry
    nr  = 0                     # number of radial grid points
    nth = 0                     # number of incl. grid points
    nph = 1
    velo_ph = np.nan            # velocity data (2 dim)
    gdens = np.nan             # gas density (2 dim)
    tgas = np.nan              # gas temperature (2 dim)
    moldata = np.nan           # number density of emitting species(2 dim)
    colldat = np.nan           # number density of collisional partner (e.g. H2)
    molspec = ''               # RT species names, string or list of strings
    nmol = 0
    
    dustkappa_fname = None
    
    car_x = None
    car_z = None
    car_tgas = None
    
    spectra = None
    
    # Stellar parameters
    Rstar = None
    Mstar = None
    Tstar = None
    
    def __init__(self, data, molspec='nH2O', method='nearest', 
                 nr=None, nth=None):
        
        self.params = data['params']
        self.molspec = [molspec]
        self.nmol = np.size(molspec)

        # Old cartesian grid and cell centres in (rho,th) coordinates
        car_x = data['r'] * au
        car_z = data['z'] * au
        
        # Store Cartesian data
        self.car_x = car_x
        self.car_z = car_z
        self.car_tgas = data['Tgas']
    
        # Stellar parameters
        self.Tstar = self.params['T_star']
        self.Rstar = np.sqrt(self.params['L_star'] / ls * ts**4 / 
                             self.Tstar**4) * rs
        self.Mstar = self.params['M_star'] * ms

        # New grid - interface centred
        if not nr:
            self.nr = car_x.shape[0]
        else:
            self.nr = nr
        if not nth:
            self.nth = car_x.shape[1]
        else:
            self.nth = nth

        sph_r_min = car_x.min()
        sph_th_max = 0.5 * np.pi
        
        if sph_r_min < 1.3 * self.Rstar:
            sph_r_min = 1.3 * self.Rstar
        sph_th_max = np.arccos( self.Rstar / sph_r_min)

        self.ri = 10**np.linspace(np.log10(sph_r_min), 
                                  np.log10(car_x.max()), 
                                  self.nr+1)
        self.thi = np.linspace(0.0, sph_th_max, self.nth+1)

        # Cell centre coordinates
        rc = (self.ri[1:] + self.ri[0:-1]) / 2.0
        thc = (self.thi[1:] + self.thi[0:-1]) / 2.0

        # Cell centred coordinates in Cartesian system
        self.car_rc = rc[:, None] * np.cos(thc[None, :])
        self.car_zc = rc[:, None] * np.sin(thc[None, :])
    
        # Interpolate data to spherical coordinates
        self.tgas  = self.car2sph(car_x, car_z, self.car_rc, self.car_zc, 
                                 data['Tgas'], method=method)
        self.tgas[np.where(self.tgas == 0.)] = 0.1 # avoid 0 division in RADMC-3D
        
        self.gdens = self.car2sph(car_x, car_z, self.car_rc, self.car_zc,
                                  data['nH'], method=method)
        # Remove this once the correct mass is used
        self.gdens = self.gdens / 1.0E+045
        
        self.moldata = np.empty( (self.nr, self.nth, self.nmol) )
        for i in range(self.nmol):
            self.moldata[:,:,i]  = self.car2sph(car_x, car_z, self.car_rc, 
                                                self.car_zc, 
                                                data[self.molspec[i]], 
                                                method=method)
        
        # Compute the disk rotational velocity
        self.velo_ph = self.kepler_velo(self.car_rc)
        
        # Set wavelength grid
        self.lgrange=[-1,4]
        self.nl=100
        self.wavelength = np.logspace(np.min(self.lgrange), 
                                      np.max(self.lgrange), 
                                      num=self.nl, endpoint=True)
        
        
    def car2sph(self, x, y, r, z, values, method='nearest'):
        '''
        Interpolates data from regular Cartesian- to regular spherical 
        coordinates using the nearest neighbour, linear or cubic method.
        
        The x,y arguments should give the regular Cartesian grid on which 
        the values are given. The r,z arguments should give the Cartesian 
        coordinates of the new spherical grid points.
        
        The method assumes cell centred grid and data definition.
    
        Parameters
        ----------
        x       - radial coordinate of original Cartesian grid 
        y       - height coordinate of original Cartesian grid
        r       - Cartesian radial coordinate of the new spherical grid
        z       - Cartesian height coordinate of the new spherical grid
        values  - data to be mapped to new grid
        method  - string selecting interpolation method (linear, cubic or nearest)
    
        Returns
        -------
        ipol_values - data mapped to the spherical grid
        '''
        points = (x.ravel(), y.ravel())
        newgrid = (r, z)
        values[np.isnan(values)] = 0.0
    
        ipol_values = griddata(points, values.ravel(), newgrid, method=method)

        return ipol_values

    def kepler_velo(self, r_car, Mstar=None):
        '''
        Computes angular velocity of Keplerian orbits.
    
        Parameters
        ----------
        r_car - disk plane projected radial distance from centre of gravity in cm
        Mstar - mass of centre of gravity in solar masses (optional)
        
        Returns
        -------
        velo  - scalar or array containing the local angular velocity
        '''
        if not Mstar:
            Mstar = self.Mstar / ms

        velo = np.sqrt( gg * Mstar * ms / r_car )

        return velo
    
    def plot_model(self, xrange=[0,20], yrange=[0,20], vrange=None, cmap=None):
        '''
        Plots diagnostic figures regarding the interpolation and internally 
        computed physical conditions.
        
        Interpolation window compares the original Cartesian and interpolated 
        (on spherical grid) temperature maps.
        Keplerian rotation window show the radial dependence of the angular 
        velocity, representing Keplerian rotation.
        
        Note that all previously open plotting windows will be closed.
        
        Parameters
        ----------
        (x,y,v)range - radial, height and colour plotting ranges
        cmap         - colour map to be used
        '''
        plt.close('all')
        plt.ion()               # switch to interactive mode

        # Show original and interpolated temperature data
        fig , ax  = plt.subplots(1, 2, sharey=False, figsize=[8.5,6])
        fig.canvas.set_window_title('Interpolation')
        
        for i in range(2):
            if (i == 0) :
                x = self.car_x / au
                y = self.car_z / au
                data = self.car_tgas
                xlabel = "r$_\mathrm{car}$ [au]"
                ylabel = "z$_\mathrm{car}$ [au]"
            elif (1 == 1):
                x = self.car_rc / au
                y = self.car_zc / au
                data = self.tgas
                xlabel = 'r$_\mathrm{sph}$ [au]'
                ylabel = 'z$_\mathrm{car}$ [au]'
            
            ax[i].set_xlabel(xlabel)
            ax[i].set_ylabel(ylabel)
        
            if vrange:
                vmin = min(vrange)
                vmax = max(vrange)
            else:
                vmin = data.min()
                vmax = data.max()

            pan = ax[i].pcolormesh(x, y, data, vmin=vmin, vmax=vmax, cmap=cmap)
            ax[i].set_xlim(xrange)
            ax[i].set_ylim(yrange)

            cb1 = plt.colorbar(pan, ax=ax[i], cmap=cmap, orientation='horizontal')
            cb1.set_label('Temperature [K]')
        
        # Show mid-plane velocity data
        fig, ax = plt.subplots(1, 1, sharey=False, figsize=[6,6])
        fig.canvas.set_window_title('Keplerian rotation')
        
        ax.plot(self.car_rc[:,0]/au, self.velo_ph[:,0]/1.0e5)
        ax.set_yscale('log')
        ax.set_xlabel('r$_\mathrm{sph}$ [au]')
        ax.set_ylabel('$v_{\Phi}$ [km/s]')
        ax.set_title('Stellar mass: {} Msol'.format(self.params['M_star']))
        
        plt.show()
        
        return

    def write2disk(self, spec='nH2O', folder='.', incl_dust=False, 
                   incl_line=True):
        '''
        Write RADMC-3D model to given folder.
        
        Parameters
        ----------
        spec      - name of species to be written (as stored in input dictionary)
        folder    - output is written to this location
        incl_dust - control parameter to allow dust temperature and/or add 
                    dust continuum to spectra (changes param. in radmc3d.inp)
                    (default False)
        incl_line - control parameter to allow molecular line computation. If 
                    set to false, then lines will not be included in SED, image
                    and spectra calculation. (default True)
        '''

        # Write amr_grid.inp
        self._write_amr_grid(folder=folder)
        
        # Write gas temperature and dust density
        self._write_scalar(self.tgas, fname='gas_temperature.inp', folder=folder)
        self._write_scalar(self.gdens/(2.3*mp)/100., fname='dust_density.inp', 
                           folder=folder)
        
        # Write number density files
        if type(spec) == str:
            spec = [spec]
        
        for s in spec:
            mi = self.molspec.index(s)
            s_ = s.replace('n','').lower()
            if s_ in op_spec.keys():
                self._write_scalar(self.moldata[:,:,mi]*op_spec[s_], 
                                   folder=folder, 
                                   fname='numberdens_{}{}.inp'.format('o',s_))
                self._write_scalar(self.moldata[:,:,mi]*(1.0-op_spec[s_]),
                                   folder=folder, 
                                   fname='numberdens_{}{}.inp'.format('p',s_))
            else:
                self._write_scalar(self.moldata[:,:,mi], folder=folder, 
                                   fname='numberdens_{}.inp'.format(s_))
        
        # Write velocity field
        self._write_vector(self.velo_ph, fname='gas_velocity.inp', folder=folder)
        
        # Write dustkappa file
        # Requires network connection to download files!
        try:
            self.get_dustkappa_osshen(spec='mrn', cden=0, folder=folder)
        except:
            print('WARN: dustkappa file was not written!')
        try:
            self.get_moldata(spec=spec, folder=folder)
        except:
            print('WARN: molecular data was not written!')
        
        # Write control/options file
        self._write_radmc3dinp(folder=folder, incl_dust=incl_dust, 
                               incl_line=incl_line)
        self._write_lines(folder=folder, spec=spec)
        self._write_dustopac(folder=folder)
        self._write_wavelength(folder=folder)
        self._write_starsinp(folder=folder)
        
        return

    def get_spectrum(self, imolspec=1, iline=1, incl=45, posang=0, phi=0, 
                     widthkms=10, nlam=40, mol='oh2o'):
        '''
        Computes and reads spectrum using RADMC-3D
        
        The function uses the makeSpectrum() radmc3dPy method (not currently 
        part of package, see below) to compute a spectrum of molecule "mol" and 
        line transition 'iline'. The RADMC-3D model has to be already written 
        to the disk (current location) for this function to work.
        
        Note: function requires radmc3dPy.
        
        Parameters
        ----------
        iline  -   index of line transition to be computed
        incl   -   model inclination in deg (defaults to 45 deg)
        posang -   model position angle in deg (default is 0)
        phi    -   model ... angle on the sky (default is 0)
        widthkms - spectra is computed in this velocity range around the central 
                   wavelength (default is 10 km/s)
        nlam   -   
        mol    -   lower case string containing the molecule name (e.g. oh2o)
        
        Note that imolspec and mol parameters must be changed together: imolspec
        1 refers to mol='oh2o', 2 refers to mol='ph2o'.
        
        '''
        makeSpectrum(iline=iline, incl=incl, posang=posang, phi=phi,
                     widthkms=widthkms, nlam=nlam, imolspec=imolspec)
        
        spectrum_ = radmc3dPy.analyze.readSpectrum()
        
        mdat = None
        if mol:
            mdat = radmc3dPy.molecule.radmc3dMolecule()
            mdat.read(mol=mol)
        
        spec_dict = {'spectrum': spectrum_, 'iline': iline,
                     'incl': incl, 'posang':posang, 'phi':phi,
                     'widthkms':widthkms, 'mol':mdat}
        
        if self.spectra is None:
            self.spectra = [spec_dict]
        else:
            self.spectra.append(spec_dict)
        
        return
    
    def plot_spectrum(self, index=0, dpc=1.0, mol=None, iline=None, **kwargs):
        '''
        Plots selected spectra on screen. 
        '''
        if self.spectra is None:
            print('WARN: No spectra found!')
            return -1
        elif len(self.spectra) > 1 and index == 0:
            print('INFO: multiple spectra found. Using index=0')
        if not mol:
            mol = self.spectra[index]['mol']
        
        if not iline:
            iline = self.spectra[index]['iline']
        
        if mol == None and iline != None:
            iline = None
            
        radmc3dPy.analyze.plotSpectrum(self.spectra[index]['spectrum'], dpc=dpc,
                                       mol=mol, ilin=iline, **kwargs)
        
        return
        
    def _write_scalar(self, data, folder='.', fname=None):
        '''
        Write scalar quantities to RADMC-3D input file format. Typically the 
        dust / gas density and temperature and molecular number densities are
        written by this function. The input data must be 2D with (nr * nth) size.
        
        Parameters
        ----------
        data    -  2D scalar data to be written (nr * nth size)
        folder  -  output is written to this location
        fname   -  name of output file (e.g. dust_density.inp)
        '''
        data[np.where(np.isnan(data))] = 0.0
        
        out = "{}/{}".format(folder.strip(), fname.strip())
        fout = open(out,'w')
        #
        # Header
        fout.write('{:8n}\n'.format(1))                         # Format number
        fout.write('{:8n}\n'.format(self.nr*self.nth*self.nph)) # Nr of cells
        fout.write('{:8n}\n'.format(1))                         # Nr of dust species
        #
        # Data
        data = data[::,::-1]
        
        for ip in range(self.nph):
            for it in range(self.nth):
                for ir in range(self.nr):
                    fout.write('{:11.4E}\n'.format(data[ir,it]))

        fout.close()
        
        return
    
    def _write_vector(self, data, folder='.', fname=None):
        '''
        Write vector quantities to RADMC-3D input file format. Currently the 
        function supports only the velocity data in spherical coordinates and 
        vr = 0 and vth = 0 is assumed. (the only velocity component is the 
        vphi Keplerian rotation).
        
        Parameters
        ----------
        data    -  2D data to be written (nr * nth size), containing vphi
        folder  -  output is written to this location
        fname   -  name of output file (e.g. gas_velocity.inp)
        
        Returns
        -------
        Vector like quantity written to [folder]/[fname] file. 
        '''
        data = data[::,::-1]
        
        out = "{}/{}".format(folder.strip(), fname.strip())
        fout = open(out,'w')
        #
        # Header
        fout.write('{:8n}\n'.format(1))                         # Format number
        fout.write('{:8n}\n'.format(self.nr*self.nth*self.nph)) # Nr of cells
        fout.write('{:8n}\n'.format(1))                         # Nr of dust species
        #
        # Data
        for ip in range(self.nph):
            for it in range(self.nth):
                for ir in range(self.nr):
                    fout.write('{:11.4E} {:11.4E} {:11.4E}\n'.format(0,0,data[ir,it]))

        fout.close()
        
        return

    def _write_dustopac(self, folder='.'):
        '''
        Write dust opacity control file dustopac.inp. The file defines the number, 
        style and file name of used dust opacity files.
        
        Parameters
        ----------
        folder  -  output is written to this location
        
        Returns
        -------
        dustopac.inp - RADMC-3D dust opacity control file
        '''
        ds = self.dustkappa_fname.replace('.inp','').replace('dustkappa_','')
        
        out = "{}/{}".format(folder.strip(), 'dustopac.inp')
        fout = open(out,'w')
        fout.write('{:8n}\n'.format(2))
        fout.write('{:8n}\n'.format(1))                 # number of dust species
        fout.write('-----------------------------\n')
        fout.write('{:8n}\n'.format(1))                 # input file style
        fout.write('{:8n}\n'.format(0))
        fout.write('{}\n'.format(ds))   # opacity file name
        fout.write('-----------------------------\n')
        fout.close()

        return

    def _write_amr_grid(self, folder='.'):
        '''
        Write amr_grid.inp RADMC-3D input file. The file contains the spherical 
        (iformat=100) model grid (r, theta, phi).
        
        Parameters
        ----------
        folder  -  output is written to this location
        
        Returns
        -------
        amr_grid.inp - RADMC-3D grid input file
        '''
        out = "{}/{}".format(folder.strip(), 'amr_grid.inp')

        fout = open(out, 'w')
        #
        # Header
        fout.write('{:8n}\n'.format(1))   # iformat
        fout.write('{:8n}\n'.format(0))   # AMR grid style (0=regular grid, no AMR)
        fout.write('{:8n}\n'.format(100)) # Coordinate system (100 spherical)
        fout.write('{:8n}\n'.format(0))   # gridinfo
        fout.write('{:8n}  {:8n}  {:8n}\n'.format(1,1,0)) # Include x,y,z coordinate
        fout.write('{:8n}  {:8n}  {:8n}\n'.format(self.nr,self.nth,self.nph))
        #
        # Write cell interface coordinates
        for r in self.ri:
            fout.write('{:11.6E}\n'.format(r))
        for th in self.thi:
            fout.write('{:11.6E}\n'.format(th)) 
        for ph in self.phi:
            fout.write('{:11.6E}\n'.format(ph))
        fout.close()
        
        return
        
    def _write_wavelength(self, folder='.', lgrange=[-1,4], nl=100):
        '''
        Write wavelength_micron.inp RADMC3D input file. The file contains the 
        model wavelength grid. This is needed for all RADMC3D computations.
        
        Parameters
        ----------
        folder  -  output is written to this location
        lgrange -  logarithm of wavelength range extreme values (list or array).
                   The extremes are included in the range.
        nl      -  number of wavelength points in lgrange
        '''
        if (nl != self.nl) or (lgrange != self.lgrange):
            lam = np.logspace(np.min(lgrange), np.max(lgrange), 
                              num=nl, endpoint=True)
            # Save wavelength grid
            self.nl = nl
            self.lgrange = lgrange
            self.wavelength = lam
        else:
            lam = self.wavelength

        out = "{}/{}".format(folder.strip(), 'wavelength_micron.inp')
        fout = open(out,'w')
        fout.write('{:8n}\n'.format(nl))
        for i in range(nl):
            fout.write('{:11.6E}\n'.format(lam[i]))
        fout.close()
        
        return

    def _write_lines(self, folder='.', spec=None, sp_mode=1):
        '''
        Write lines.inp RADMC3D input file. This is needed for line transfer. 
        Currently all models are set up for LTE computation. For non-LTE models
        besides the number density of the modelled species, the number density
        of collisional partners need to be provided.
        
        Parameters
        ----------
        folder  -  output is written to this location
        spec    -  (list) of species to be included in line transfer.
                   Note that numberdens_[spec].inp must exist in folder.
        '''
        spec_ = []
        if len(spec) == str:
            spec = [spec]
        
        for s in spec:
            ss = s.replace('n','').lower()
            # If isomers or deuterated forms exist, then species name should be 
            # expanded here
            if ss in op_spec.keys():
                spec_.append('o{}'.format(ss))
                spec_.append('p{}'.format(ss))
            else:
                spec_.append(ss)
        
        out = "{}/{}".format(folder.strip(), 'lines.inp')
        fout = open(out,'w')
        fout.write('{:8n}\n'.format(2))
        fout.write('{:8n}\n'.format(len(spec_)))   # number of modelled species
        # modelled species, mol. data style, lower and upper transition, 
        # nr. collisional partners (0 == LTE)
        for s in spec_:
            fout.write('{}  {}  {:8n}  {:8n}  {:8n}\n'.format(s, 'leiden', 
                                                              0, 0, 0))
            fout.write('{}\n{}\n'.format("",""))         # collisional partners
        fout.close()
        
        return
    
    def _write_radmc3dinp(self, rt_params=None, folder='.', incl_dust=False, 
                          incl_line=True, lines_mode=1):
        '''
        Writes the main RADMC3D parameter file, radmc3d.inp to the specified 
        folder.
        
        Parameters
        ----------
        rt_params  -  dictionary containing parameter-value pairs
                      (see Appendix A.1 in RADMC-3D manual)
        incl_dust  -  control parameter to allow dust temperature and/or add 
                      dust continuum to spectra (changes param. in radmc3d.inp)
                      (default False). This option overwrites the incl_dust
                      parameter in rt_params!
        incl_line  -  control parameter to allow molecular line computation. If 
                      set to False, then lines will not be included in SED, image
                      and spectra calculation. (default True) This option 
                      overwrites the incl_line parameter in rt_params!
        lines_mode -  Line radiative transfer mode (1: LTE, 3: LVG, 4: 
        '''
        if not rt_params:
            rt_params = {'incl_dust': 0,
                    'incl_lines': 1,
                    'incl_freefree': 0,
                    'nphot_spec': 10000,
                    'rto_style': 1,        # 1 ASCII, 2 F77 unformatted, 3 binary
                    'lines_mode': 1,       # 1 LTE, 
                    'lines_show_pictograms': 0,
                    'modified_random_walk': 1,
                    'istar_sphere': 1}
        
        if incl_dust:
            rt_params['incl_dust'] = 1
        if incl_line:
            rt_params['incl_lines'] = 1
        
        self.rt_params = rt_params
        
        out = "{}/{}".format(folder.strip(), 'radmc3d.inp')
        fout = open(out,'w')
        for par in rt_params.keys():
            fout.write('{} = {}\n'.format(par, rt_params[par]))
        fout.close()
        
        return

    def _write_starsinp(self, folder='.', pos=(0., 0., 0.)):
        '''
        Writes stars.inp RADMC-3D input file containing the radius, temperature 
        and position of the radiation source. Note this file is only needed 
        for computing the dust temperature using RADMC-3D. For SED and spectra 
        computation using the model input model temperatures, this file is not 
        needed.
        
        Parameters
        ----------
        pos      -  Cartesian coordinates of star (typically [0,0,0])
        folder   -  output is written to this location (optional)
        '''
        pos_x, pos_y, pos_z = pos
        
        out = "{}/{}".format(folder.strip(), 'stars.inp')
        fout = open(out,'w')
        
        fout.write('{:8n}\n'.format(2))
        fout.write('{:8n} {:8n}\n'.format(1,self.nl))
        fout.write('{:11.6E} {:11.6E} {:11.6E} {:11.6E} {:11.6E}\n'.format(
                  self.Rstar,self.Mstar,pos_x,pos_y,pos_z))
        for i in range(self.nl):
            fout.write('{:11.6E}\n'.format(self.wavelength[i]))
        fout.write('{:11.6E}\n'.format(-self.Tstar))
        fout.close()
        
        return

    def get_moldata(self, folder='.', filename=None, spec='nH2O'):
        '''
        Download molecular data tables from the Leiden database (see link 
        below). These files are required for line radiative transfer with 
        RADMC-3D. 
        
        Parameters
        ----------
        folder   -  output is written to this location (optional)
        filename -  Leiden data table file name as is on their website (optional)
        spec     -  name of modelled species
        '''
        
        # Set predefined file names
        spec_file = {'h2o': ['ph2o@daniel.dat', 'oh2o@daniel.dat'],
                     'n2h+':['n2h+_hfs.dat']}
        
        if not filename:
            if type(spec) == list:
                if len(spec) > 1:
                    print('INFO: list species parameter found, using first element only!')
                spec = spec[0]
            spec_l = spec.replace('n','').lower()
            if spec_l in spec_file.keys():
                filename = spec_file[spec_l]
            else:
                filename = ['{}.dat'.format(spec_l)]
        else:
            if type(filename) == str:
                filename = [filename]
        
        url_base = 'https://home.strw.leidenuniv.nl/~moldata/datafiles/'
        moldata = []
        for ff in filename:
            url_tabl = '{}/{}'.format(url_base,ff)
            # read molecular data tmp file
            dl = urllib.request.urlretrieve(url_tabl)
            dlf = open(dl[0],'r')
            dld = dlf.readlines()
            dlf.close()
            os.remove(dl[0])
            # write file to model folder
            moldata.append( 'molecule_{}.inp'.format(
                            ff.split('.')[0].split('_')[0].split('@')[0] ) )
            wdf = open(moldata[-1], 'w')
            wdf.writelines(dld)
            wdf.close()

        # Save file names to class variable
        self.moldata_fname = moldata

        return
        
    def get_dustkappa_osshen(self, folder='.', spec='mrn', cden=0):
        '''
        Download opacity tables from Ossenkopf & Henning (1994). These 
        are computed for ISM dust, therefore may not be applicable to dust in 
        protoplanetary disks. However, the file is needed for RADMC-3D to 
        function.
        
        The user may select between mrn / thin / thick models and 
        coagulation densities of 0 / 5 / 6 / 7 / 8. 
        
        Parameters
        ----------
        folder  -  output is written to this location
        spec    -  dust type (mrn/thin/thick == bare/thin ice/thick ice)
        cden    -  coagulation density (0/5/6/7/8)
        
        Returns
        -------
        dustkappa_{spec}{cden}.dat - RADMC-3D readable dust opacity input file 
        '''
        filename = '{}{}'.format(spec, cden)
        url_base = 'https://hera.ph1.uni-koeln.de/~ossk/Jena/tables/'
        url_tabl = '{}/{}'.format(url_base,filename)
        
        dl = urllib.request.urlretrieve(url_tabl)
        
        # Read temp file and write dust opacity file
        lk = np.loadtxt(dl[0])
        os.remove(dl[0])
        
        fname = 'dustkappa_osshen_{}{}.inp'.format(spec,cden)
        out = "{}/{}".format(folder.strip(), fname.strip())
        
        fout = open(out,'w')
        fout.write('{:8n}\n'.format(1)) 
        fout.write('{:8n}\n'.format(lk.shape[0]+1)) 
        fout.write('{:11.4E} {:11.4E}\n'.format(0.1, lk[0,1])) 
        for varr in lk:
            fout.write('{:11.4E} {:11.4E}\n'.format(varr[0], varr[1])) 
        fout.close()
        
        # Save file name to class variable
        self.dustkappa_fname = fname
        
        return 0
    
    
#
# makeSpectra should be merged to radmc3dPy
# 

def makeSpectrum(npix=None, incl=None, wav=None, phi=None, posang=None, 
                 pointau=None, fluxcons=True, nostar=False, noscat=False, 
                 widthkms=None, linenlam=None, vkms=None, iline=None, 
                 lambdarange=None, nlam=None, stokes=False, binary=False,
                 imolspec=None):
    """Calculates a spectrum with RADMC-3D 

    Parameters
    ----------

    incl        : float
                  Inclination angle of the source

    wav         : float
                  Wavelength of the image in micron

    phi         : float, optional
                  Azimuthal rotation angle of the source in the model space

    posang      : float, optional
                  Position angle of the source in the image plane

    pointau     : Float, optional
                  Three elements list of the cartesian coordinates of the image center

    widthkms    : float, optional
                  Width of the frequency axis of the channel maps

    linenlam    : int, optional
                  Number of wavelengths to calculate images at

    vkms        : float, optional
                  A single velocity value at which a channel map should be calculated

    iline       : int, optional
                  Line transition index

    lambdarange : list, optional
                  Two element list with the wavelenght boundaries between which
                  multiwavelength images should be calculated

    nlam        : int, optional
                  Number of wavelengths to be calculated in lambdarange

    fluxcons    : bool, optional
                  This should not even be a keyword argument, it ensures flux conservation 
                  (adaptive subpixeling) in the rectangular images

    nostar      : bool, optional
                  If True the calculated images will not contain stellar emission

    noscat      : bool, optional
                  If True, scattered emission will be neglected in the source function, however, 
                   extinction will contain scattering if kappa_scat is not zero.  

    stokes      : bool, optional
                  If True, images in all four stokes parameters (IQUV) will be calculated, if
                  False only the intensity will be calculated

    binary      : bool, optional
                  If True the output image will be written in a C-style binary format, if False
                  the image format will be ASCII
                  
    imolspec    : int, optional
                  If more than one species is given in lines.inp, then this 
                  parameter selects the modelled species. By default the first
                  species is modelled.

    Example
    -------

    makeSpectrum(incl=60.0, iline=2, phi=0., posang=15., pointau=[0., 0.,0.], 
                 fluxcons=True, nostar=False, noscat=False)

    """
    #
    # The basic keywords that should be set
    #
    if incl is None:
        msg = 'Unkonwn incl. Inclination angle must be set.'
        raise ValueError(msg)

    if wav is None:
        if (lambdarange is None) & (nlam is None):
            if vkms is None:
                if (widthkms is None) & (linenlam is None):
                    msg = 'Neither wavelength nor velocity is specified at which the image should be calculated'
                    raise ValueError(msg)
                else:
                    if iline is None:
                        msg = 'Unknown iline. widthkms, linenlam keywords are set indicating that a line '\
                              + 'channel map should be calculated, but the iline keyword is not specified'
                        raise ValueError(msg)
            else:
                if iline is None:
                    msg = 'Unknown iline. vkms keyword is set indicating that a line channel map should be'\
                          + 'calculated, but the iline keyword is not specified'
                    raise ValueError(msg)
    else:
        if lambdarange is not None:
            msg = 'Either lambdarange or wav should be set but not both'
            raise ValueError(msg)

    if lambdarange is not None:
        if len(lambdarange) != 2:
            msg = 'lambdarange must have two and only two elements'
            raise ValueError(msg)

    #
    # Kees' fix for the case when a locally compiled radmc3d exists in the current directory
    #
    com = ''
    if os.path.isfile('radmc3d'):
        com = com + './'
    com = com + 'radmc3d spectrum'

    # com = 'radmc3d image'
    com = com + ' incl ' + str(incl)

    if wav is not None:
        com = com + ' lambda ' + str(wav)
    elif (lambdarange is not None) & (nlam is not None):
        com = com + ' lambdarange ' + str(lambdarange[0]) + ' ' + str(lambdarange[1]) + ' nlam ' + str(int(nlam))
    elif vkms is not None:
        com = com + ' vkms ' + str(vkms)
    elif (widthkms is not None) & (linenlam is not None):
        com = com + ' widthkms ' + str(widthkms) + ' linenlam ' + str(linenlam)
    elif (widthkms is not None) & (iline is not None):
        com = com + ' widthkms ' + str(widthkms) + ' iline ' + ("%d" % iline)

    #
    # Now add additional optional keywords/arguments
    #
    if phi is not None:
        com = com + ' phi ' + str(phi)

    if posang is not None:
        com = com + ' posang ' + str(posang)

    if pointau is not None:
        if len(pointau) != 3:
            msg = ' pointau should be a list of 3 elements corresponding to the  cartesian coordinates of the ' \
                  + 'image center'
            raise ValueError(msg)
        else:
            com = com + ' pointau ' + str(pointau[0]) + ' ' + str(pointau[1]) + ' ' + str(pointau[2])
    else:
        com = com + ' pointau 0.0  0.0  0.0'

    if fluxcons:
        com = com + ' fluxcons'

    if stokes:
        com = com + ' stokes'

    if binary:
        com = com + ' imageunform'

    if nostar:
        com = com + ' nostar'

    if noscat:
        com = com + ' noscat'

    if imolspec:
        com = com + ' imolspec ' + ("%d" % imolspec)

    #
    # Now finally run radmc3d and calculate the image
    #
    # dum = sp.Popen([com], stdout=sp.PIPE, shell=True).wait()
    dum = sp.Popen([com], shell=True).wait()
    
    print(com)

    return 0
