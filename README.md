# Quick-start Guide

Note that most methods contain help information, which describes the available 
parameters and their meaning.

List of user facing methods:

* plot_model     -   Show / compare model in Cartesian and spherical systems 
* write2disk     -   Write model to folder
* get_spectrum   -   Compute spectra and read in as class variable
                     (radmc3dmodel.spectra)
* plot_spectrum  -   Plot spectra already contained in class variable

Potentially useful methods (used internally):

* get_moldata    -   Download excitation and collisional data from Leiden
                     database
* get_dustkappa_osshen - Download dust opacity data from Ossenkopf & Henning 
                         (1994)
* car2sph        -   Convert Cartesian data to spherical data
* kepler_velo    -   Set Keplerian velocity in disk

## Requirements

* Python 3.7
* Numpy >1.15
* Matplotlib >2.2
* SciPy >1.1
* [radmc3dPy](https://www.ast.cam.ac.uk/~juhasz/radmc3dPyDoc/) >0.30.2
* [RADMC-3D](http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/) >0.41

## How to use

### Create model

```
data  = np.load('TTau_1513822087_H2O_forLaszlo.npy').item()
model = convert2radmc3d.radmc3dmodel(data, nr=300, nth=1000, method='nearest')
```

Important parameters:

* method - set interpolation method which is used to convert model from 
           Cartesian to spherical coordinate system.

* nr and nth - number of radial and polar angle grid points in the spherical
               coordinate system. To represent the Cartesian data in spherical
               coordinates accurately, a larger number of grid points is needed.

### Plot model and write to folder

```
model.plot_model()
model.write2disk(spec='nH2O', folder='.', incl_dust=False, incl_line=True)
```

The incl_dust and incl_line parameters set whether dust continuum and line emission are modelled or not. If incl_dust is set to True, then the dust temperature has to be computed outside of the python module, i.e. in command line:

`radmc3d mctherm setthreads 4`

### Compute and plot spectra

As an example compute spectra in 10 km/s range around the ortho-H2O (J=2-1) line 
and disk inclination of 60 deg. Scale the brightness of the plotted spectra, as 
it is seen at 150 parsec distance.

```
model.get_spectrum(iline=2, incl=60, nlam=100, mol='oh2o', widthkms=10)
model.plot_spectrum(dpc=150)
```

The computed spectra is stored in the spectra class variable as a list. The elements of the list are dictionaries containing the spectra and additional meta-data (multiple execution of the get_spectra() method populates this list). To select between list elements, use the index parameter in plot_spectrum().

## Caveats

### RADMC-3D error messages about photons outside of grid.

This error occurs when the inner grid points (in Cartesian coordinates used for 
the photon transfer) intersect the stellar surface (if istar_sphere=1, default).
If the stellar radiation source is point source, then the error might still occur, if the inner grid points are too close to the axis origin. During the initialization of the radmc3dmodel class checks are performed to avoid this issue.

### Lack of volume density data

The "TTau_1513822087_H2O_forLaszlo.npy" file does not contain volume density data. This would be needed to set the dust density for the thermal Monte Carlo and the dust continuum emission computations. To substitute the data, I take the 'nH' key from the input file dictionary and scale it to about 5.3E-3 Msol total mass (that the disk is not too optically thick).

Furthermore, the volume density (or better the H2 volume density) is needed for 
non-LTE line computations. In the simple LVG case, when the major collisional 
partner of the modelled molecule is H2, then the volume density is used to 
compute the ortho and para H2 number density.

Once the correct volume density data is available in the input, please modify 
the following lines in convert2radcm3d.py:

```
self.gdens = self.car2sph(car_x, car_z, self.car_rc, self.car_zc,
                          data['nH'], method=method)
# Remove this once the correct mass is used
self.gdens = self.gdens / 1.0E+045
```

### Non-keplerian velocity

In the current version the line position and width is determined by Doppler shift due to Keplerian disk rotation and thermal line broadening. No (micro) turbulent 
broadening is modelled.

### Realistic line profile and flux

To model the line emission realistically, the dust continuum emission, photon 
scattering on dust grains and absorption by dust have to be included in the 
radiative transfer computation. Because no dust density data is provided, at the current stage these processes should be switched off.

### Inconsistent between imolspec and mol

Parameter iline in get_spectrum() method is used to select the central wavelength of the modelled spectrum in a user friendly mode. Instead of remembering the wavelength of all lines, the user can specify the index of molecular transition and its rest frequency will be used as central wavelength. When more than one molecules are defined in lines.inp (e.g. in case of ortho-H2O and para-H2O), then the imolspec parameter is used to choose which line to use for the transition based central wavelength selection. Note that if the lines of multiple modelled molecules fall into the modelled frequency range, then all those lines are automatically included.

The central wavelength need to be stored in the self.spectra class variable in 
order to show the velocity shift on the x-axis with plot_spectrum() method. This 
is stored in a radmc3dMolecule class variable (radmc3dPy). The mol string parameter is used in get_spectrum() to specify the molecular data file name that 
is used to read in the radmc3dMolecule data.

For the correct operation of this program, the user *must set imolspec and mol 
simultaneously to their desired / correct value*. E.g. if we model the spectrum 
around a ortho-H2O line, then imolspec = 1, mol = 'oh2o', if we model para-H2O, then imolspec = 2, mol = 'ph2o'.
